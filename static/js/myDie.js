// as a class of a canvas


let cornerPts = (tableArray) => {
  //   console.table(tableArray);

  var [ x_min, y_min] = tableArray[0].slice(1);
  var [ x_max, y_max] = tableArray[0].slice(1);

  for (elem_i of tableArray) {
    var [ x_i, y_i] = elem_i.slice(1);
    if (x_i < x_min) x_min = x_i;
    if (y_i < y_min) y_min = y_i;
    if (x_i > x_max) x_max = x_i;
    if (y_i > y_max) y_max = y_i;
  }

  return [x_min, y_min, x_max, y_max];
};



function OneDie(objId) {
    var canvas = document.getElementById(objId);
    var cxt = canvas.getContext('2d');
    var [imgW, imgH] = [canvas.width + 50, canvas.height + 50];
    // this.coordToDraw = null;
    // this.canvasSize = null;
    // console.log(`width : ${imgW}<br> height : ${imgH}`);


    this.draw = function (ptsCoordArray) {


        // background
        cxt.fillStyle = "rgba(166,164,162,0.26)";
        cxt.fillRect(0, 0, imgW, imgH);
        // cxt.fillRect(x_min, y_min, x_max- x_min, y_max-y_max);
        cxt.beginPath();
        // cxt.strokeStyle = "rgba(4,14,120,0.88)";
        // cxt.stroke();
        [x_min, y_min, x_max, y_max] = cornerPts(ptsCoordArray);
        cxt.fillRect(x_min,y_min, )

        cxt.fillStyle = "red";
        cxt.font = "12px verdana";
        cxt.textAlign = "left";


        $.each(ptsCoordArray, function (index, elem) {
            if (elem.includes(null)) return
            [_mod, _x, _y] = elem;
            cxt.strokeRect(_x, _y, 5, 5);

            cxt.fillText(`${_mod}, (${_x}, ${_y})`, _x + 10, _y + 10);
        })


    }


    // this._cornerPts = (allPts) => {
    //     let [x_min, y_min] = allPts[0];
    //     let [x_max, y_max] = allPts[0];
    //     for (idx in theArray) {
    //         var theCoor = theArray[idx];
    //         [tmp_x, tmp_y] = theCoor;
    //
    //         if (tmp_x < x_min) x_min = tmp_x;
    //         if (tmp_x > x_max) x_max = tmp_x;
    //         if (tmp_y < y_min) y_min = tmp_y;
    //         if (tmp_y > y_max) y_max = tmp_y;
    //     }
    //     return [x_min,y_min,x_max,y_max]
    //
    // }

    this.drawPoints = (coordArray) => {
        cxt.fillStyle = "red";
        cxt.font = "12px verdana";
        cxt.textAlign = "left";
        $.each(coordArray, function (index, elem) {
            if (elem.includes(null)) return
            [_mod, _x, _y] = elem;
            cxt.strokeRect(_x, _y, 5, 5);

            cxt.fillText(`${_mod}, (${_x}, ${_y})`, _x + 10, _y + 10);
        })


    }
}