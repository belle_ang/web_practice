import mariadb

from flask import Blueprint, request, render_template, session, redirect

acc = Blueprint("acc", __name__)
dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")


@acc.route("/login_page", methods=["POST", "GET"])
def login_page():
    return render_template("login.html")


@acc.route("/register_page", methods=["POST", "GET"])
def register_page():
    return render_template("register.html")


def _is_user_exist(user_name, user_pwd):
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute(
        f"SELECT username, password FROM tmp.users WHERE (username ='{user_name}') AND (password ='{user_pwd}')"
    )
    result = cursor.fetchone()
    return False if result is None else True


@acc.route("/register", methods=["POST", "GET"])
def register():
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    user_name = request.form.get("userName")  # 如果用args會抓不到, 新增空的一行
    user_pwd = request.form.get("userPassword")

    if _is_user_exist(user_name, user_pwd) is False:
        cursor.execute(f"INSERT INTO tmp.users(username , password) VALUES('{user_name}', '{user_pwd}')")
        dbconn.commit()
        session["username"] = user_name
        return render_template("four.html")
    else:

        return render_template("login.html", _status="a")


@acc.route("/login_check", methods=["POST", "GET"])
def login_check():
    user_name = request.form.get("userName")
    user_pwd = request.form.get("userPassword")
    if _is_user_exist(user_name, user_pwd) is True:
        session["username"] = user_name
        return render_template("four.html")
    else:
        return render_template("register.html", _status="User not found")


@acc.route("/logout", methods=["POST", "GET"])
def logout():
    session["username"] = None
    return redirect("login_page")
