import re
from flask import Flask, render_template, request, session, redirect
from four import db_four
from accounts import acc
from wafer import wafer

app = Flask(__name__)
app.register_blueprint(db_four, url_prefix="/db_four")
app.register_blueprint(acc, url_prefix="/acc")
app.register_blueprint(wafer, url_prefix="/wafer")

app.config["SECRET_KEY"] = "abcd"


@app.route("/")
@app.route("/1")
def one():
    return render_template("one.html")


@app.route("/2")
def two():
    return render_template('two.html', var1="app_return_tezt")


@app.route("/3")
def three():
    return render_template("three.html")


@app.route("/4")
def four():
    return render_template("four.html")


@app.route("/convertObject", methods=["GET", "POST"])
def convertObject():
    print("ooxx")
    # 資料可以成功送出去; 資料怎麼收進來
    f_name = request.form.get("filename")
    print("*** read :", f_name)
    return f"server reply:'{f_name}'"


@app.route("/db_query", methods=["GET", "POST"])
def data_query():
    _cmd = request.form.get("_cmd")
    # print ( f"\n***{datetime.datetime.now()}- {_cmd}\n")
    # print ("***" +_data)
    return f"app.py return : <{_cmd}>"


@app.before_request
def login_auth():
    _general_path = ["/acc/login_page", "/acc/login_check", "/acc/register", "/acc/register_page"]
    if not (request.path in _general_path or "/static/" in request.path) and session.get("username") is None:
        return redirect("/acc/login_page")

    # _general_path = ["/acc/login_page", "/acc/login_check", "/acc/register", "/acc/register_page"]
    # if request.path in _general_path or "/static/" in request.path:
    #     print("PASSING CASE")
    #     return None
    # else:
    #     if session.get("username") is None:
    #         print(session.get("username"))
    #         print("USER IS NONE")
    #         return redirect("/acc/login_page")


if __name__ == "__main__":
    app.run(debug=True, port="1122")
