from flask import Blueprint, render_template, request

wafer = Blueprint('wafer', __name__)


@wafer.route('wafer1', methods=['POST', 'GET'])
def wafer1():
    return render_template('wafer/wafer1.html')


@wafer.route('wafer2', methods=['POST', 'GET'])
def wafer2():
    return render_template('wafer/wafer2.html')


@wafer.route('wafer3', methods=['POST', 'GET'])
def wafer3():
    return render_template('wafer/wafer3.html')


@wafer.route('wafer4', methods=['POST', 'GET'])
def wafer4():
    return render_template('wafer/wafer4.html')


@wafer.route('dieMap', methods=['POST', 'GET'])
def dieMap():
    return render_template('wafer/dieMap.html')

@wafer.route('gototable', methods=['GET', 'POST'])
def gototable():
    return render_template('wafer/handsontable.html', newName=request.args.get('newName'))
